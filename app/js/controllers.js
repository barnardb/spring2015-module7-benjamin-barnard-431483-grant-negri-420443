'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

function indexOf(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i].name == obj.name) {
            return i;
        }
    }
    return -1;
}

phonecatControllers.controller('PhoneListCtrl', ['$scope', '$rootScope','Phone',
  function($scope, $rootScope, Phone) {
    $scope.phones = Phone.query();
    $scope.orderProp = 'age';
    $rootScope.selected = [];
    $scope.addToSel = function(id) {
		Phone.get({phoneId:id}, function(phone) {
			var ind = indexOf($rootScope.selected, phone);
			if(ind == -1){
				$rootScope.selected.push(phone);
			} else {
				$rootScope.selected.splice(ind, 1);
			}
		});
    }
  }]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);
  
phonecatControllers.controller('PhoneCompCtrl', ['$scope', '$rootScope', 'Phone',
	function($scope, $rootScope, Phone) {
}]);
